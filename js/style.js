let buttons = document.querySelectorAll(".btn");

function deleteClass() {
  buttons.forEach((item) => item.classList.remove("active-button"));
}

addEventListener("keydown", function (e) {
  function func(x) {
    x.classList.contains("active-button")
      ? x.classList.remove("active-button")
      : x.classList.add("active-button");
  }

  for (let i of buttons) {
    if (e.code.includes("Key" + i.textContent)) {
      deleteClass();
      func(i);
    }
    if (e.code.includes("NumpadEnter")) {
      deleteClass();
      func(buttons[0]);
    }
  }
});

